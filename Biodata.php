<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 1</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body class="biru">
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.html">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="biodata.html">Biodata</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="berita.html">Berita</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="galeri.html">Galeri</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
    <div class="row">
        <div class="cold-md-3">
            <div class="Card">    
                <div class="card-body">
                    <img src="images.png" class="img-fluid" alt="Fikri Haiqal">
                </div>
            </div>
        </div>
        <div class="cold-md-9">
            <div class="card">
            </div class="card-body">
            <table class="table table-hover table-warning" border">
                <tbody>
                    <center><H2>BIODATA DIRI</H2></center>
                </tr>
                <tr>
                    <td>NPM</td>
                    <td>:</td>
                    <td>021220039</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>:</td>
                    <td>Fikri Haiqal</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>Laki Laki</td>
                    <tr>
                    </tr>
                    <td>Tempat Tanggal Lahir</td>
                    <td>:</td>
                    <td>Palembang, 23 Juni 2002</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>KOMP Bumi mas indah blok B2</td>
                </tr>
                <tr>
                    <td>Hobi</td>
                    <td>:</td>
                    <td>
                        <ul>
                            <li>Hilling</li>
                            <li>Game</li>
                            <li>Tidur</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>Sekolah</td>
                    <td>:</td>
                    <td>
                        <ol>
                            <li>SD Negeri 16 Talang Bungin</li>
                            <li>SMP Negeri 1 Musi Landas</li>
                            <li>SMK Negeri 7 Palembang</li>
                        </ol>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

</body>
</html>